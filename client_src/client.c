#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#define MAX 1025
#define PORT 8080
#define TRUE 1
#define SA struct sockaddr

void chat(int sockfd)
{
	char buff[MAX];
	int n;
	while(TRUE){
        bzero(buff, sizeof(buff));
		printf("\nEnter the string : ");
		n = 0;
        //olvasunk a konzolról amíg entert nem észlelünk
		while ((buff[n++] = getchar()) != '\n');
		write(sockfd, buff, sizeof(buff));
		bzero(buff, sizeof(buff));
		//beolvassuk mi érkezett a szerverről
		read(sockfd, buff, sizeof(buff));
		printf("From Server : %s", buff);
		if ((strncmp(buff, "quit", 4)) == 0) {
			printf("Client Quit...\n");
			break;
		}
	}
}

int main()
{
	int sockfd, connfd;
	struct sockaddr_in servaddr, cli;

	// megpróbálja létrehozni a socketet és ellenőrzi hogy sikerült-e
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1) {
		printf("socket creation failed...\n");
		exit(0);
	}
	else
		printf("Socket successfully created..\n");
	bzero(&servaddr, sizeof(servaddr));

	// hozzárendelem az ip-t és a portot a serveraddresshez
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servaddr.sin_port = htons(PORT);

	// a kliens socketet csatlakoztatjuk a serverhez
	if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) {
		printf("connection with the server failed...\n");
		exit(0);
	}
	else
		printf("connected to the server..\n");

    char message[MAX];
    read(sockfd, message, sizeof(message));

	// function a chathez
	chat(sockfd);

	// socket bezárása
	close(sockfd);
}
