#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <check.h>
#include "mock_client.h"
#define MAX 1025
#define PORT 8080
#define TRUE 1
#define SA struct sockaddr
int socket_creation_connect(){
     int sockfd;
	struct sockaddr_in servaddr, cli;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1) {
		printf("socket creation failed...\n");
		exit(0);
	}
	else
		printf("Socket successfully created..\n");
	bzero(&servaddr, sizeof(servaddr));

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servaddr.sin_port = htons(PORT);

	if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) {
		printf("connection with the server failed...\n");
		exit(0);
	}
	else
		printf("connected to the server..\n");

    char message[MAX];
    read(sockfd, message, sizeof(message));
    return sockfd;

}

char* sendcommand(char* command,int sockfd){
    char message[1025];
   send(sockfd, command, strlen(command),0);
	bzero(message, sizeof(message));
    read(sockfd, message, 1024);
  if(message!=NULL){
     int len=strlen(message);
	 len+=1;
     char* value=malloc(len);
    strncpy(value,message,len);
    return value;
  }else{
      return "NULL";
  }
}