#include <stdlib.h>
#include <stdio.h>
#include <check.h>
#include "mock_client.c"
extern int sock_val,connect_val;
extern int socketfd=0;

    START_TEST(test_socket)
    {
        ck_assert_int_ne(socketfd,-1);
    }
    END_TEST

    START_TEST(test_every_command)
 {
    char* message="put hold on\n";
    char* buff=sendcommand(message,socketfd);
    ck_assert_str_eq(buff,"OK");
    message="get hold\n";
    buff=sendcommand(message,socketfd);
    ck_assert_str_eq(buff,"on");
    message="del hold\n";
    buff=sendcommand(message,socketfd);
    ck_assert_str_eq(buff,"OK");
    message="del sio\n";
    buff=sendcommand(message,socketfd);
    ck_assert_str_eq(buff,"OK");
    message="list-keys\n";
    buff=sendcommand(message,socketfd);
    ck_assert_str_eq(buff,"sys\n");
    message="put sio barack\n";
    buff=sendcommand(message,socketfd);
    ck_assert_str_eq(buff,"OK");
    message="quit\n";
    buff=sendcommand(message,socketfd);
    ck_assert_str_eq(buff,"quit\n");
    close(socketfd);
 }
 END_TEST

 START_TEST(test_put)
    {
        char *message,*buff,*key,*value,*tmp;
        int keylen=strlen("key");
        int valuelen=strlen("value");
        for(int i=0;i<50;i++){
            key=malloc(keylen+1+sizeof(i));
            strcpy(key,"key");
            tmp=malloc(sizeof(i));
            sprintf(tmp, "%d", i);
            strcat(key,tmp);
            sprintf(key,"%s%s",key," ");


            value=malloc(valuelen+2+sizeof(i));
            strcpy(value,"value");
            strcat(value,tmp);
            strcat(value,"\n");
            
            message=malloc(strlen("put ")+strlen(key)+strlen(value));
            strcpy(message,"put ");
            strcat(message,key);
            strcat(message,value);
            
            buff=sendcommand(message,socketfd);
            
            ck_assert_str_eq(buff,"OK");
        }
    }
END_TEST

 START_TEST(test_get)
    {
        char *message,*buff,*key,*value,*tmp;
        int keylen=strlen("key");
        int valuelen=strlen("value");
        for(int i=0;i<50;i++){
            key=malloc(keylen+2+sizeof(i));
            strcpy(key,"key");
            tmp=malloc(sizeof(i));
            sprintf(tmp, "%d", i);
            strcat(key,tmp);
            strcat(key,"\n");


            value=malloc(valuelen+2+sizeof(i));
            strcpy(value,"value");
            strcat(value,tmp);
            
            message=malloc(strlen("get ")+strlen(key));
            strcpy(message,"get ");
            strcat(message,key);

            
            buff=sendcommand(message,socketfd);
            ck_assert_str_eq(buff,value);
        }
    }
END_TEST

 
 START_TEST(test_del)
    {
        char *message,*buff,*key,*tmp;
        int keylen=strlen("key");
        for(int i=0;i<50;i++){
            key=malloc(keylen+2+sizeof(i));
            strcpy(key,"key");
            tmp=malloc(sizeof(i));
            sprintf(tmp, "%d", i);
            strcat(key,tmp);
            strcat(key,"\n");
           // sprintf(key,"%s%s",key," ");
            
            message=malloc(strlen("del ")+strlen(key));
            strcpy(message,"del ");
            strcat(message,key);

            buff=sendcommand(message,socketfd);
            ck_assert_str_eq(buff,"OK");
        }
    }
END_TEST

Suite * socket_suite(void)
{
    Suite *s;
    TCase *tc_core,*tc_commands;
    socketfd=socket_creation_connect();
    s = suite_create("Socket");

    tc_core = tcase_create("socket");
    tcase_add_test(tc_core, test_socket);
    suite_add_tcase(s, tc_core);
    return s;
}

 
Suite * commands_suite(void)
{
    Suite *s;
    TCase *tc_core,*tc_commands;
    socketfd=socket_creation_connect();
    s = suite_create("Commands");
    tc_commands=tcase_create("Commands");
    tcase_add_test(tc_commands, test_every_command);
    tcase_add_test(tc_commands, test_put);
    tcase_add_test(tc_commands, test_get);
    tcase_add_test(tc_commands, test_del);
    suite_add_tcase(s, tc_commands);
    return s;
}


 int main(void)
 {
    int number_failed;
    Suite *s,*s2;
     SRunner *sr;
    
    s = socket_suite();
    s2= commands_suite();
    sr = srunner_create(s);
    srunner_add_suite(sr,s2);
    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
 }

