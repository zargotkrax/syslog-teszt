#ifndef socketCreation_H_INCLUDED
#define socketCreation_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

int socketCreation(int socketName);
void sockopt(int socketName);
void initializeSockets(int max_sockets,int sockets_to_initalize[]);
void bindSocket(int socketName,struct sockaddr_in address);
void setMaxListen(int socketName,int max);
fd_set validSockets(int max_sockets,int socketdesc,int sockets[],fd_set readfds);
int getMaxSocketDesc(int max_sockets,int socketdesc,int sockets[],int max_socketdesc);

#endif