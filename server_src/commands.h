#ifndef COMMANDS_H_INCLUDED
#define COMMANDS_H_INCLUDED
#include "string-hash-table.h"
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
enum commandNumber{error,list_keys,list_values,put,get,del,count,quit};

enum commandNumber getCommandNumber(char* buffer);
char* listKeys(HashTable* ht);
char* listValues(HashTable* ht);
char* SplitString(char* StringToSplit,char* Delimiters,int IsFirstSplit);
char* putIntoHash(HashTable *ht,char* buffer);
char* getFromHash(HashTable* ht,char* buffer);
char* delFromHash(HashTable* ht,char* buffer);
char* CountOfPairs(HashTable* ht,char* buffer);
#endif 