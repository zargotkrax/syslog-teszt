#include "commands.h"
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "hash-table.h"

int opt=1;


enum commandNumber getCommandNumber(char* buffer)
{
    
    enum commandNumber value;
     
    if (strncmp("list-keys", buffer, 9) == 0)
    {
        value=list_keys;
    }else if (strncmp("list-values", buffer, 11) == 0)
    {   
        value=list_values;
    }else if (strncmp("put",buffer,3)==0){
        value=put;
    }else if (strncmp("get",buffer,3)==0){
        value=get;
    }else if (strncmp("del",buffer,3)==0){
        value=del;
    }else if (strncmp("count", buffer, 5) == 0) {
        value=count;
    }else if (strncmp("quit", buffer, 4) == 0) {
        value=quit;
    }else{
        value=error;
    }
    return value;
}
char* listKeys(HashTable *ht)
{
    char* buffer=malloc(1);
    unsigned int index=_idx_by_key(ht,"sys");
    char* current_key;
    int i=0;
      while(i<hash_table_size(ht)){
          if(ht->keyValues.keys[index]!=NULL){
            i++;
            current_key=ht->keyValues.keys[index];
            strcat(buffer,current_key);
            strcat(buffer,"\n");
          }
          index = (index + 1) % hash_table_capacity(ht);
      }
        buffer[strlen(buffer)]='\0';
    return buffer;
}

char* listValues(HashTable *ht)
{
    char* buffer=malloc(1);
    unsigned int index=_idx_by_key(ht,"sys");
    char* current_value;
    int i=0;
      while(i<hash_table_size(ht)){
          if(ht->keyValues.values[index]!=NULL){
            i++;
            current_value=ht->keyValues.values[index];
            strcat(buffer,current_value);
            strcat(buffer,"\n");
          }
          index = (index + 1) % hash_table_capacity(ht);
      }
        buffer[strlen(buffer)]='\0';
    return buffer;
    
}

char* SplitString(char* StringToSplit,char* Delimiters,int IsFirstSplit){
    char* tmp,*returnValue;
    if(IsFirstSplit==1){
        tmp=strtok(StringToSplit,Delimiters);
    }
    tmp=strtok(NULL,Delimiters);
    if(tmp!=NULL)
    {
        int len=strlen(tmp);
        returnValue=malloc(len);
        strncpy(returnValue,tmp,len);
        return returnValue;
    }else{
        return NULL;
    }
    
}

char* putIntoHash(HashTable *ht,char* buffer)
{
    char* delimiters=" =";
    char* uzenet,*key,*value;
    printf("%s%s\n","buffer",buffer);
    key=SplitString(buffer,delimiters,1);
    value=SplitString(buffer,delimiters,0);

    if(key !=NULL && value !=NULL){
        int len=strlen(value);
        value[len-1]='\0';
        hash_table_put( ht, key,value);
        uzenet="OK";
    }else{
        uzenet="a beszúráshoz 3 szó kell a parancs majd a kulcs majd a value";
    }
    return uzenet;
}

char* getFromHash(HashTable* ht,char* buffer)
{
    char* delimiters=" =";
    char* uzenet,*key;
    key=SplitString(buffer,delimiters,1);
    if(key!=NULL){
        int len=strlen(key);
        key[len-1]='\0';
        if(hash_table_get(ht,key)!=NULL){
            uzenet=hash_table_get(ht,key);  
        }else{
            uzenet="nincs ilyen kulcs";
        }
    }else{
        uzenet="get után írjon be egy szót";
    }
    return uzenet;
}

char* delFromHash(HashTable* ht,char* buffer)
{
    char* delimiters=" =";
    char* uzenet,*key;
    key=SplitString(buffer,delimiters,1);
    if(key!=NULL){
        int len=strlen(key);
        key[len-1]='\0';
        if(hash_table_get(ht,key)!=NULL){
            hash_table_delete(ht,key);  
            uzenet="OK";
        }else{
            uzenet="nincs ilyen kulcs";
        }
    }else{
        uzenet="get után írjon be egy szót";
    }
    return uzenet;
}

char* CountOfPairs(HashTable* ht,char* buffer){
    char* uzenet;
    unsigned int value=hash_table_size(ht);
    uzenet=malloc(sizeof(value));
    sprintf(uzenet, "%u", value);
    return uzenet;
}
