#include "connectionHandler.h"
#include "commands.h"
#include "socketCreation.h"
#include <errno.h>
connectionHandler* connectionHandler_new(int max_connections,int master_socket,struct sockaddr_in address,HashTable* ht){
    connectionHandler *self = NULL;
    if ((max_connections <= 0))
    return NULL;

    self = (connectionHandler *)calloc(1, sizeof(connectionHandler));
    self->max_connections=max_connections;
    self->master_socket=master_socket;
    self->address=address;
    self->client_socket = (int  *)calloc(max_connections, sizeof(int *));
    self->addrlen=sizeof(address);
    self->message="the connection was successfull";
    self->ht=ht;
    return self;
}

void new_connection(connectionHandler* self){
        int new_socket;
    if((new_socket=accept(self->master_socket,(struct sockaddr*) &self->address,(socklen_t*)& self->addrlen))<0){
                perror("accept");
                exit(EXIT_FAILURE);
            }
            printf("New connection , socket fd is %d , ip is : %s , port : %d \n" , new_socket , inet_ntoa(self->address.sin_addr) , ntohs
				(self->address.sin_port));

            
            if( send(new_socket, self->message, strlen(self->message), 0) != strlen(self->message) )  
            {  
                perror("sending");  
            }  
             printf("%s\n","Sending was successfull"); 

            int j=0;
            
            while(self->client_socket[j]!=0)
            {
                j++;
            }
            printf("Adding to list of sockets as %d\n" , j); 
            self->client_socket[j]=new_socket;
}

void check_Connections(connectionHandler* self,fd_set readfds)
{
    for(int i=0;i<self->max_connections;i++)
        {
            
            bzero(self->buffer,sizeof(self->buffer));
            self->socketdesc=self->client_socket[i];
            if(FD_ISSET(self->socketdesc,&readfds))
            {
                if((self->valread=read(self->socketdesc,self->buffer,1024))==0)
                {
                    getpeername(self->socketdesc , (struct sockaddr*)&self->address ,(socklen_t*)&self->addrlen); 
                    printf("Host disconnected , ip %s , port %d \n" , inet_ntoa(self->address.sin_addr) , ntohs(self->address.sin_port));  
                            
                    close(self->socketdesc);
                    self->client_socket[i]=0;
                }else
                {
                    char* uzenet;
                    self->buffer[self->valread]='\0';
                    self->commandnumber=getCommandNumber(self->buffer);
                    
                    switch(self->commandnumber)
                    {
                        case error:
                            if(strlen(self->buffer)!=0)
                            {
                                uzenet="nincs ilyen parancs/ne rakj szóközt a parancs elé";
                                
                            }
                        break;
                        case list_keys:
                            uzenet=listKeys(self->ht);
                            printf("%s\n","listing keys");
                        break;
                        case list_values:
                            uzenet=listValues(self->ht);
                            printf("%s\n","listing values");
                        break;
                        case put:
                            uzenet=putIntoHash(self->ht,self->buffer);
                            printf("%s\n","put into hash");
                        break;
                        case get:
                            uzenet=getFromHash(self->ht,self->buffer);
                            printf("%s\n","get from hash");
                        break;
                        case del:
                            uzenet=delFromHash(self->ht,self->buffer);
                            printf("%s\n","deleted from hash");
                        break;
                        case count:
                            uzenet=CountOfPairs(self->ht,self->buffer);
                            printf("%s\n","count of pairs");
                        break;
                        case quit:
                            uzenet="quit\n";
                            printf("Client quitting from server...\n");
                        break;
                    }
                    
                    send(self->socketdesc, uzenet, strlen(uzenet),0);
                    while(strlen(self->buffer)!=0){
                        read(self->socketdesc,self->buffer,1024);
                        memset(self->buffer,0,sizeof(self->buffer));
                    }
                    
                    memset(self->buffer,0,sizeof(self->buffer)); 
                }
            }
    }
    
}

void checkSockets(connectionHandler* self){
    FD_ZERO(&self->readfds);
    FD_SET(self->master_socket,&self->readfds);
    self->max_socketdesc=self->master_socket; 
    self->readfds=validSockets(self->max_connections,self->socketdesc,self->client_socket,self->readfds);
    self->max_socketdesc=getMaxSocketDesc(self->max_connections,self->socketdesc,self->client_socket,self->max_socketdesc);
}

void chatting(connectionHandler* self){
    while(1)
    {
        checkSockets(self);
        self->activity=select(self->max_socketdesc+1,&self->readfds,NULL,NULL,NULL);

        if ((self->activity < 0) && (errno!=EINTR))  
        {  
            printf("select error");  
        }  
        if(FD_ISSET(self->master_socket,&self->readfds))
        {
            new_connection(self);
        }
        check_Connections(self,self->readfds);
    }
}