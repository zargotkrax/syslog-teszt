#include "socketCreation.h"

int socketCreation(int socketName){
    if((socketName=socket(AF_INET,SOCK_STREAM,0))==0){
        perror("socket creation ");
        exit(EXIT_FAILURE);
    }
    return socketName;
}

void sockopt(int socketName){
     if(setsockopt(socketName, SOL_SOCKET, SO_REUSEADDR,(char *)&opt,sizeof(opt))<0)
     {
        perror("setsockopt");  
        exit(EXIT_FAILURE); 
     }
}

void initializeSockets(int max_sockets,int sockets_to_initalize[]){
    for(int i=0;i<max_sockets;i++){
        sockets_to_initalize[i]=0;
    }
}

void bindSocket(int socketName,struct sockaddr_in address){
    if(bind(socketName,(struct sockaddr* )&address,sizeof(address))<0){
        perror("bind");
        exit(EXIT_FAILURE);
    }
}

void setMaxListen(int socketName,int max){
    if (listen(socketName, max) < 0)  
    {  
        perror("listening");  
        exit(EXIT_FAILURE);  
    } 
}

fd_set validSockets(int max_sockets,int socketdesc,int sockets[],fd_set readfds){
        for(int i=0;i<max_sockets;i++){
            socketdesc=sockets[i];

            if(socketdesc>0)
            {
                FD_SET(socketdesc,&readfds);
            }
        }
        return readfds;
}

int getMaxSocketDesc(int max_sockets,int socketdesc,int sockets[],int max_socketdesc){
    for(int i=0;i<max_sockets;i++){
        socketdesc=sockets[i];
        if(socketdesc>max_socketdesc){
                max_socketdesc=socketdesc;
            }
        
    }
    return max_socketdesc;
}