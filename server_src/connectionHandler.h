#ifndef CONNECTIONHANDLER_H_INCLUDED
#define CONNECTIONHANDLER_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "hash-table.h"

typedef struct _connectionHandler connectionHandler;
struct _connectionHandler{
    int max_connections, master_socket,valread,socketdesc,addrlen,commandnumber,max_socketdesc,activity;
    int* client_socket;
    struct sockaddr_in address;
    char* message;
    char buffer[1025];
    fd_set readfds;
    HashTable* ht;
};

connectionHandler* connectionHandler_new(int max_connections,int master_socket,struct sockaddr_in address,HashTable* ht);
void new_connection(connectionHandler* ch);
void check_Connections(connectionHandler* self,fd_set readfds);
void checkSockets(connectionHandler* self);
void chatting(connectionHandler* self);
#endif