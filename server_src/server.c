//https://www.geeksforgeeks.org/socket-programming-in-cc-handling-multiple-clients-on-server-without-multi-threading/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include "hash-table.c"
#include "string-hash-table.c"
#include "commands.c"
#include "socketCreation.c"
#include "connectionHandler.c"

#define TRUE 1
#define FALSE 0
#define PORT 8080
int main(int argc , char *argv[])
{
    HashTable* ht=string_hash_table_new(100);
    hash_table_put(ht,"sys","log");
    hash_table_put(ht,"sio","barack");
    
    
    int master_socket=0,max_clients=30,client_socket[30];
    struct sockaddr_in address;

    char buffer[1025];
    bzero(buffer,sizeof(buffer));

    master_socket=socketCreation(master_socket);
    initializeSockets(max_clients,client_socket);
    sockopt(master_socket);
    address.sin_family=AF_INET;
    address.sin_addr.s_addr=INADDR_ANY;
    address.sin_port=htons(PORT);
    bindSocket(master_socket,address);
    setMaxListen(master_socket,3);

    printf("%s\n","running and waiting for connection");
    connectionHandler* ch=connectionHandler_new(max_clients,master_socket,address,ht);
    chatting(ch);
    hash_table_free(ht);
return 0;
}